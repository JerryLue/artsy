# CarCar

Team:

* Jerry - Service
* Maggie - Sales

## Design

## Service microservice

Build technician and appointment app with list create and delete rest functions incorporating the automobile data from the inventory app.

## Sales microservice

Create Salesperson API, Customer API, and Sales API by writing model and view codes that support an application used by an automobile dealership, complete with front-end user friendly applications.
