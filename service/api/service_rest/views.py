from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    TechnicianListEncoder,
    TechnicianDetailEncoder,
    AppointmentListEncoder,
    AppointmentDetailEncoder,
)
from .models import Technician, Appointment, AutomobileVO


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            AppointmentListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            try:
                AutomobileVO.objects.get(vin=content["vin"])
                content["vip_status"] = True
            except AutomobileVO.DoesNotExist:
                content["vip_status"] = False
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)
        except Exception as e:
            return JsonResponse(
                {"message": ("Could not make appointment: ", e)},
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, id):
    if request.method == "GET":
        try:
            auto = Technician.objects.get(id=id)
            return JsonResponse(auto, encoder=TechnicianDetailEncoder, safe=False)
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = Technician.objects.get(id=id)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            auto = Technician.objects.get(id=id)
            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment(request, id):
    if request.method == "GET":
        try:
            auto = Appointment.objects.get(id=id)
            return JsonResponse(auto, encoder=AppointmentDetailEncoder, safe=False)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = Appointment.objects.get(id=id)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            auto = Appointment.objects.get(id=id)
            props = [
                "date",
                "time",
                "reason",
                "vin",
                "customer",
                "technician",
            ]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.canceled()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.finished()
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_search_vin(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.filter(vin=id)
            return JsonResponse(
                appointment, encoder=AppointmentDetailEncoder, safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
