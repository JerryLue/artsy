import React, { useEffect, useState } from "react";


function Customers(){
    const [customers, setCustomers] = useState([]);

    const loadCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log("data from customers:", data)
            setCustomers(data.customer);
        }
    };

    useEffect(() => {
        loadCustomers();
    }, []);

    
    return(
        <div className="px-4 py-5 my-5 text-left">
        <h1>Customers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
            {customers.map(customer => {
                return (
                    <tr key={customer.id}>
                        <td>{customer.first_name}</td>
                        <td>{customer.last_name}</td>
                        <td>{customer.phone_number}</td>
                        <td>{customer.address}</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
        </div>
    )
}


export default Customers;
