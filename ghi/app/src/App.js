import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListTechnicians from "./ListTechnicians";
import AddTechnicians from "./AddTechnician";
import AddAppointment from "./AddAppointment";
import HistoryAppointment from "./HistoryAppointment";
import Salespeople from "./Salespeople";
import AddaSalesperson from "./AddaSalesperson";
import Customers from "./Customers";
import AddaCustomer from "./AddaCustomer";
import Sales from "./Sales";
import AddaSale from "./AddaSale";
import SalesHistory from "./SalesHistory";
import ListManufacturers from "./ListManufacturers";
import ListModels from "./ListModels";
import ListAutomobiles from "./ListAutomobiles";
import Appointments from "./ListAppointments";
import AddAutomobiles from "./AddAutomobile";
import ListAppointments from "./ListAppointments";
import AddaManufacturer from "./AddaManufacturer";
import AddaModel from "./AddaModel";

function App() {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="technicians/" element={<ListTechnicians />} />
                    <Route
                        path="technicians/create"
                        element={<AddTechnicians />}
                    />
                    <Route
                        path="appointments/create/"
                        element={<AddAppointment />}
                    />
                    <Route
                        path="appointments/history/"
                        element={<HistoryAppointment />}
                    />
                    <Route path="salespeople/" element={<Salespeople />} />
                    <Route
                        path="salespeople/create"
                        element={<AddaSalesperson />}
                    />
                    <Route path="customers/" element={<Customers />} />
                    <Route path="customers/create" element={<AddaCustomer />} />
                    <Route path="sales/" element={<Sales />} />
                    <Route path="sales/create" element={<AddaSale />} />
                    <Route path="sales/history" element={<SalesHistory />} />
                    <Route
                        path="manufacturers/"
                        element={<ListManufacturers />}
                    />
                    <Route path="models/" element={<ListModels />} />
                    <Route path="automobiles/" element={<ListAutomobiles />} />
                    <Route
                        path="automobiles/create"
                        element={<AddAutomobiles />}
                    />
                    <Route path="appointments/" element={<Appointments />} />
                    <Route
                        path="appointments/"
                        element={<ListAppointments />}
                    />
                    <Route
                        path="manufacturers/create"
                        element={<AddaManufacturer />}
                    />
                    <Route path="models/create" element={<AddaModel />} />
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
