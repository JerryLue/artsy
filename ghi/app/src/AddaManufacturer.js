import React, { useEffect, useState } from "react";

function AddaManufacturer() {
    const [manufacturerName, setManufacturerName] = useState("");

    const handleManufacturerNameChange = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = manufacturerName;

        const manufacturerUrl = `http://localhost:8100/api/manufacturers/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            setManufacturerName("");
        }
    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a manufacturer</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={manufacturerName}
                                onChange={handleManufacturerNameChange}
                                placeholder="Manufacturer name..."
                                required
                                type="text"
                                id="manufacturer_name"
                                name="manufacturer_name"
                                className="form-control"
                            />
                            <label htmlFor="manufacturer_name">
                                Manufacturer Name
                            </label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AddaManufacturer;
