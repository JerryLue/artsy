import React, { useEffect, useState } from "react";

function AddAppointment() {
    const [techniciansList, setTechniciansList] = useState([]);
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [date, setDate] = useState("");
    const [time, setTime] = useState("");
    const [technician, setTechnician] = useState("");
    const [reason, setReason] = useState("");

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    };
    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    };
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    };
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    };

    const techniciansListData = async () => {
        const technicianUrl = "http://localhost:8080/api/technicians/";
        const response = await fetch(technicianUrl);

        if (response.ok) {
            const data = await response.json();
            setTechniciansList(data.technician);
        }
    };

    useEffect(() => {
        techniciansListData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date = date;
        data.time = time;
        data.technician = technician;
        data.reason = reason;
        const appointmentsUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            setVin("");
            setCustomer("");
            setDate("");
            setTime("");
            setTechnician("");
            setReason("");
        }
    };
    return (
        <div className="row">
            <div>
                <h1>Create a service appointment</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input
                            value={vin}
                            onChange={handleVinChange}
                            placeholder=""
                            required
                            type="text"
                            id="vin"
                            name="vin"
                            className="form-control"
                        />
                        <label htmlFor="vin">Automobile Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={customer}
                            onChange={handleCustomerChange}
                            placeholder=""
                            required
                            type="text"
                            id="customer"
                            name="customer"
                            className="form-control"
                        />
                        <label htmlFor="customer">Customer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={date}
                            onChange={handleDateChange}
                            placeholder="mm/dd/yyyy"
                            required
                            type="date"
                            id="date"
                            name="date"
                            className="form-control"
                        />
                        <label htmlFor="">Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={time}
                            onChange={handleTimeChange}
                            placeholder="--:-- --"
                            required
                            type="time"
                            id="time"
                            name="time"
                            className="form-control"
                        />
                        <label htmlFor="time">Time</label>
                    </div>
                    <div className="mb-3">
                        <select
                            value={technician}
                            onChange={handleTechnicianChange}
                            placeholder="Choose a technician..."
                            required
                            id="technician"
                            name="technician"
                            className="form-select"
                        >
                            <option value="technicians">Technician</option>
                            {techniciansList.map((technician) => {
                                return (
                                    <option
                                        key={technician.id}
                                        value={technician.employee_id}
                                    >
                                        {technician.first_name}{" "}
                                        {technician.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={reason}
                            onChange={handleReasonChange}
                            placeholder=""
                            type="textarea"
                            id="reason"
                            name="reason"
                            className="form-control"
                        />
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    );
}

export default AddAppointment;
