import React, { useEffect, useState } from "react";

function AddaModel() {
    const [modelName, setModelName] = useState("");
    const [picture, setPicture] = useState("");
    const [manufacturer, setManufacturer] = useState("");
    const [manufacturers, setManufacturers] = useState([]);

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    };

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    };

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    };

    const loadManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        loadManufacturers();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = modelName;
        data.picture_url = picture;
        data.manufacturer_id = manufacturer;

        const modelsUrl = `http://localhost:8100/api/models/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(modelsUrl, fetchConfig);
        if (response.ok) {
            setModelName("");
            setPicture("");
            setManufacturer("");
        }
    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={modelName}
                                onChange={handleModelNameChange}
                                placeholder="Model name..."
                                required
                                type="text"
                                id="model_name"
                                name="model_name"
                                className="form-control"
                            />
                            <label htmlFor="model_name">Model name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={picture}
                                onChange={handlePictureChange}
                                placeholder="Picture URL..."
                                required
                                type="text"
                                id="picture"
                                name="picture"
                                className="form-control"
                            />
                            <label htmlFor="picture">Picture URL...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={handleManufacturerChange}
                                value={manufacturer}
                                required
                                name="manfacturer"
                                id="manufacturer"
                                className="form-select"
                            >
                                <option value="">
                                    Choose an Manufacturer...
                                </option>
                                {manufacturers.map((manufacturer) => {
                                    return (
                                        <option
                                            key={manufacturer.id}
                                            value={manufacturer.id}
                                        >
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AddaModel;
