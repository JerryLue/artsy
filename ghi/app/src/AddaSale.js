
import React, {useEffect, useState } from 'react';


function AddaSale() {
    const [autos, setAutos] = useState([]);
    const [auto, setAuto] = useState("");
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState("");
    const [customers, setCustomers] = useState([]);
    const [customer, setCustomer] = useState("");
    const [price, setPrice] = useState("");

    const handleAutoChange = (event) => {
        const value = event.target.value;
        setAuto(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

	const loadAutos = async () => {
		const url = "http://localhost:8100/api/automobiles/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAutos(data.autos);
		}
	};

    useEffect(() => {
		loadAutos();
	}, []);

    const loadSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    };

    useEffect(() => {
        loadSalespeople();
    }, []);

    const loadCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customer);
        }
    };

    useEffect(() => {
        loadCustomers();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = auto;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;
        const saleUrl = `http://localhost:8090/api/sales/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        
        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            setAuto('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        };
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <select onChange={handleAutoChange} value={auto} required name="auto" id="auto" className="form-select">
                                <option value="">Choose an Automobile VIN...</option>
                                {autos.map(auto => {
                                    return(
                                        <option value={auto.vin} key={auto.vin}>{auto.vin}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a salesperson...</option>
                                {salespeople.map(salesperson => {
                                    return(
                                        <option value={salesperson.employee_id} key={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {customers.map(customer => {
                                    return(
                                        <option value={customer.first_name} key={customer.first_name}>{customer.first_name} {customer.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={price} onChange={handlePriceChange} placeholder="Price" required type="text" id="price" name="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}


export default AddaSale
