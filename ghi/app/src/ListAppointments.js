import React, { useEffect, useState } from "react";

function Appointments() {
    const [appointments, setAppointments] = useState([]);
    const loadAppointments = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointment);
        }
    };
    useEffect(() => {
        loadAppointments();
    }, []);
    const cancelClick = async (event, id) => {
        event.preventDefault();
        const appointmentCancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const fetchConfig = {
            method: "put",
        };
        const cancelResponse = await fetch(appointmentCancelUrl, fetchConfig);
        if (cancelResponse.ok) {
            setAppointments(
                appointments.filter((appointment) => appointment.id !== id)
            );
        }
    };
    const finishClick = async (event, id) => {
        event.preventDefault();
        const appointmentFinishUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
        const fetchConfig = {
            method: "put",
        };
        const finishResponse = await fetch(appointmentFinishUrl, fetchConfig);
        if (finishResponse.ok) {
            setAppointments(
                appointments.filter((appointment) => appointment.id !== id)
            );
        }
    };
    return (
        <div className="px-4 py-5 my-5 text-left">
            <h1>Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                        .filter(
                            (appointment) => appointment.status === "active"
                        )
                        .map((appointment) => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    {appointment.vip_status === false && (
                                        <td>No</td>
                                    )}
                                    {appointment.vip_status === true && (
                                        <td>Yes</td>
                                    )}
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>
                                        {appointment.technician.first_name}{" "}
                                        {appointment.technician.last_name}
                                    </td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <form>
                                            <button
                                                className="btn-outline-danger"
                                                onClick={(event) =>
                                                    cancelClick(
                                                        event,
                                                        appointment.id
                                                    )
                                                }
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="btn-outline-success"
                                                onClick={(event) =>
                                                    finishClick(
                                                        event,
                                                        appointment.id
                                                    )
                                                }
                                            >
                                                Finish
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default Appointments;
