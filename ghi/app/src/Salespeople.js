import React, { useEffect, useState } from "react";


function Salespeople(){
    const [salespeople, setSalespeople] = useState([]);

    const loadSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    };
    
    useEffect(() => {
        loadSalespeople();
    }, []);


    return(
        <div className="px-4 py-5 my-5 text-left">
        <h1>Salespeople</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
            {salespeople.map(salesperson => {
                return (
                    <tr key={salesperson.id}>
                        <td>{salesperson.employee_id}</td>
                        <td>{salesperson.first_name}</td>
                        <td>{salesperson.last_name}</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
        </div>
    )
}


export default Salespeople;
