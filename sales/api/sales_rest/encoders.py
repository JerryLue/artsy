from common.json import ModelEncoder
from .models import AutomobileVO, Salespeople, Customer, Sale


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class SalespeopleEncoder(ModelEncoder):
    model = Salespeople
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder()
    }


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder()
    }


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "salesperson",
        "customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespeopleEncoder(),
        "customer": CustomerEncoder(),
    }
